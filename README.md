# About Project Watcher

Nevada Project Watcher is a daemon to monitor changes in the root folders of the different Winner Circle projects, if it finds a change in any folder or file (modify, delete, move or creation) a custom command is triggered. Every event is recorded in a log file.

## Requirements

Python 2.7 and the [pyinotify](http://github.com/seb-m/pyinotify) library.

In Ubuntu:

    sudo apt-get install python python-pyinotify

## Configuration

There is a configuration file (config.ini) to add the different projects to monitor, add the events to analyze, exclude folders and the command that is triggered. 

If the config file is edited the daemon must restart to reload the configuration.

Example:

.. code-block:: 
    [winners-circle]

    ;Abosulute path of project
    watch=/opt/nevada/winners-circle

    ; Supported events:
    ; 'attribute_change' - Metadata changed
    ; 'create' - File/directory created
    ; 'delete' - File/directory deleted include directory itself deleted
    ; 'modify' - File was modified
    ; 'move' - File/directory moved include directory itself moved
    events=create,delete,modify,attribute_change,move

    ; Comma separated list of Absolute path of excluded folders
    excluded=/opt/nevada/winners-circle/winners_circle/vendor

    ; true - watch the directories recursively
    recursive=true

    ; true - automatically watch new subdirectory
    autoadd=true


; Wildards:
; $$  - dollar sign
; $watched  - filesystem abosulute path of project
; $filename -  abosulute path of file or directory
; $tflags - textually event flags
; $nflags - numerically event flags
command=echo $watched

## Supported events:

- attribute_change: Metadata changed
- create: File/directory created
- delete: File/directory deleted include directory itself deleted
- modify: File was modified
- move: File/directory moved include directory itself moved

## Starting the Daemon manually 

Marked as executable:

    chmod +x pwatcher.py

Start:

    ./pwatcher.py -c config.ini start

Stop

    ./pwatcher.py -c config.ini stop

Restart it with:

    ./pwatcher.py -c config.ini restart



## To boot from the start

Add to systemd:

.. code-block:: bash

    cp pwatcher.service /etc/systemd/system/pwatcher.service
    systemctl daemon-reload
    systemctl enable pwatcher
    systemctl start pwatcher